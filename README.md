# ArgoCD

# Installations Steps

## Install ArgoCD
`kubectl create namespace argocd`
`kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml`

## Wait until all is ready
`watch kubectl get pods -n argocd`

## Get initial Admin
`kubectl get secret argocd-initial-admin-secret -n argocd -o json | jq .data.password`

## Portforward the server


```
kubectl port-forward argocd-server 8080:443 -n argocd
```

By default `kubectl` binds to local interface `127.0.0.1`.

Use `--address` to provide the interface address where bind the service to

```
kubectl port-forward -n argocd services/argocd-server 8080:443 --address <IP_ADDRESS>
```

If you want to bind to all interface then use `0.0.0.0` as address

```
kubectl port-forward -n argocd services/argocd-server 8080:443 --address 0.0.0.0
```

## Open Browser
Now navigate to http://localhost:8080 . Insert as user 'admin' and as password the one decoded a few steps above.

## Download Terraform
Download Terraform for your linux distribution, in my case Fedora

Create a argocd.tf file with the following code:

```HSL
terraform {
  required_providers {
    argocd = {
      source = "oboukili/argocd"
      version = "5.0.0"
    }
  }
}

provider "argocd" {
  # Configuration options
}
```

## Configure token

## Create your first ArgoCD application

## Helm

`ArgoCD` has the native capability to work with `Helm` charts.

We can point `ArgoCD` to a chart repository (https://helm.sh/docs/topics/chart_repository/), a place where packaged charts can be stored and shared. A chart repository is at its core a static website serving an `index.yaml` and several `Helm` packages.
